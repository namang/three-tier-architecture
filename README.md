# Three Tier Architecture

![](https://mail.zoho.com/zm/ImageProxy?source=https%3A%2F%2Flh5.googleusercontent.com%2FVPk_aZrNem3UzySwOCDsoFXker4rd9aTmhnNQsOVpnXaN6l9zckeSVFylaG4EQPGlvJKl31g5enhA8zHMGBn3_V6GwGS8MEfVjV8o0CSeKWVuRk1BS6D6i8lQ3bXybdRcxkegssJ&mode=mailview)

Sets up a simple three tier architecture with vagrant vms running haproxy for load balancing, nginx and flask apps for application layer and routing and mysql for db layer.


### To run
- Install vagrant and ansible

- Clone the repo, cd into it and run
```
vagrant up

```

- Wait a long time and then everything should be accessible on the `8080` port on the host machine. Navigate to `http://localhost:8080/xyz`

- Access HAProxy stats at `http://localhost:8080/haproxy?stats`

![](./stats.png)

- Routes `/xyz, /pqrs, /abc, /mno,` are available from app_one and app_two.


##### Tested on Ubuntu 20.04.3 with Ansible v2.11.5, Python 3.8.10 and Vagrant v2.2.18