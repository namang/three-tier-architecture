from flask import Flask
import pymysql
 
app = Flask(__name__)

db = pymysql.connect(host="10.0.0.11", user="app_one_user", password="app_one", database="app_one_db")

@app.route('/xyz')
def first_route():
    return 'Route xyz\nConnected to {}, on the database {} as {}'.format(db.host, db.db.decode("utf-8"), db.user.decode("utf-8"))

@app.route('/pqrs')
def second_route():
    return 'Route pqrs'
 
 
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=6000)