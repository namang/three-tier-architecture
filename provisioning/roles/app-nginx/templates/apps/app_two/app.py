from flask import Flask
import pymysql
 
app = Flask(__name__)

db = pymysql.connect(host="10.0.0.11", user="app_two_user", password="app_two", database="app_two_db")
print(db)


@app.route('/abc')
def first_route():
    return 'Route abc\nConnected to {}, on the database {} as {}'.format(db.host, db.db.decode("utf-8"), db.user.decode("utf-8"))

@app.route('/mno')
def second_route():
    return 'Route mno'
 
 
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)